import React from "react";
// react component that copies the given text inside your clipboard
import { Radio, Form } from 'semantic-ui-react'
// reactstrap components
import {
    Card,
    CardHeader,
    CardBody,
    Container,
    Col,
    FormGroup,
    Input,
    Row, Button
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";
import moment from "moment";
import tools from "../../services/apis.js"
class EditRentClient extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            Distrubuteur: null,
            nom: null,
            prenom: null,
            nomcom: null,
            rc: null,
            check: false,
            check1: false,
            value: '',
            adresse: null,
            tel: null,
            mat: null

        }
        this.handleChange = this.handleChange.bind(this)
    }
    async componentWillMount() {
        let distrubuteur = this.props.location.state.dist

        await this.setState({
            Distrubuteur: distrubuteur, nom: distrubuteur.nom,mat:distrubuteur.matfiscal, prenom: distrubuteur.pronom, nomcom: distrubuteur.nomCommercial,
         adresse: distrubuteur.adresse, ville: distrubuteur.ville, tel: distrubuteur.telephone,rc:distrubuteur.rc
        })
    }
    async update() {
        await fetch(tools.updatedist(this.state.Distrubuteur.id), {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "nom": this.state.nom,
                "pronom": this.state.prenom,
                "matfiscal": this.state.mat,
                "rc":this.state.rc,
                "ville": this.state.ville,
                "adresse": this.state.adresse,
                "telephone": this.state.tel,
                "nomCommercial":this.state.nomcom
            }),
        })
            .then(results => {
                return results
            }).then(data => {
                console.log(data)
                this.props.history.replace("Listdist")
            })
    }
    handleChange = (e, { value }) => this.setState({ value })
    render() {
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className=" mt--7" fluid>
                    {/* Table */}
                    <Row>
                        <div className=" col">
                            <Card className=" shadow">
                                <CardHeader className=" bg-transparent">
                                    <h3 className=" mb-0">{this.state.nom + " " + this.state.prenom}</h3>
                                </CardHeader>
                                <CardBody>
                                    <Form>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup >
                                                    <Input
                                                        value={this.state.nom}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Nom"
                                                        type="text"
                                                        onChange={(text) => this.setState({ nom: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.prenom}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Prénom"
                                                        onChange={(text) => this.setState({ prenom: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.nomcom}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Nom Commercial"
                                                        onChange={(text) => this.setState({ nomcom: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.rc}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="RC"
                                                        onChange={(text) => this.setState({ rc: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.mat}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Matfiscale"
                                                        onChange={(text) => this.setState({ mat: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.ville}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Ville"
                                                        onChange={(text) => this.setState({ ville: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.adresse}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Adresse"
                                                        onChange={(text) => this.setState({ adresse: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="6">
                                                <FormGroup>
                                                    <Input
                                                        value={this.state.tel}
                                                        style={{ marginLeft: 50, width: 800, color: "#000000", fontSize: 16 }}
                                                        id="exampleFormControlInput1"
                                                        placeholder="Télephone"
                                                        onChange={(text) => this.setState({ tel: text.target.value })}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </Form>
                                    <div style={{ marginLeft: 640 }}>
                                        <Button onClick={() => this.props.history.replace("Rentclient")} color="danger" type="button"> Annuler</Button>
                                        <Button onClick={() => this.update()} color="success" type="button">Modifier</Button>
                                    </div>
                                </CardBody>
                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );
    }
}

export default EditRentClient;
