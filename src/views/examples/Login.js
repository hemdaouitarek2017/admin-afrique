/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";
import tools from "../../services/apis.js"
import { AsyncStorage } from 'AsyncStorage';
let token="token"

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      token:null
    }
  }
  async getToken(){
    try {
      const value = await localStorage.getItem(token);
      if (value !== null) {
        // We have data!
        this.setState({token:value})
      }
    } catch (error) {
      // Error retrieving data
    }
  }
  async Login() {
    await fetch(tools.signin(), {
      method: 'POST',
      body: JSON.stringify({
        usernameOrEmail: this.state.username,
        password: this.state.password
      }),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(results => {
        return results.json()
      }).then(data => {
        if (data.tokenType == "Bearer") {
          this.setToken(data.accessToken)
          this.props.history.replace(`/admin/Listdist`)
        }
        else {
          alert("Voulez-vous Verifier les informations")
        }
      })
  }
  async getRole(token) {
    console.log(token)
    await fetch(tools.getrole(this.state.username), {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      headers: {
        //this what's exactly look in my postman
        'Authorization': token
      }
    })
      .then(results => {
        return results.json()
      }).then(data => {
        if (data.Role == "ROLE_ADMIN") {
          this.props.history.replace(`/RenVehicule`)
        }
        else {
          this.props.history.replace(`/RentClientAppr`)
        }
      })
  }
  async setToken(tok){
    try {
      await localStorage.setItem(token,tok);
    } catch (error) {
      // Error saving data
    }
  }
  async componentWillMount(){
   await this.getToken()
    if(this.state.token!=null){
      this.props.history.replace(`/admin/Listdist`)

    }
  }
  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardBody style={{ height:350,backgroundColor:"#dee0e0" }} className="px-lg-5 py-lg-5">
              <Form role="form">
                <FormGroup style={{ marginTop: 50 }} className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input onChange={(text) => this.setState({ username: text.target.value })} placeholder="Utilisateur" type="email" autoComplete="new-email" />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input onChange={(text) => this.setState({ password: text.target.value })} placeholder="Mot de passe" type="password" autoComplete="new-password" />
                  </InputGroup>
                </FormGroup>
                <div className="text-center">
                  <Button style={{backgroundColor:"#fe0c00",width:348}} onClick={() => this.Login()} className="my-4" color="danger" type="button">
                    Login
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

export default Login;
