/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";

// reactstrap components
import {
    Badge,
    Card,
    CardHeader,
    Pagination,
    PaginationItem,
    PaginationLink,
    Button,
    Table,
    Container,
    Row,
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";
import tools from "../../services/apis.js"
import del from "../../assets/img/block.png"
import edit from "../../assets/img/edit.png"

class Listdist extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 0,
            ListDistrubuteur: []
        };
    }
    handleClick(e, index) {

        e.preventDefault();

        this.setState({
            currentPage: index
        });

    }
    componentWillMount() {
        this.getDistrubuteur()
        this.pageSize = 10;
    }
    async getDistrubuteur() {
        await fetch(tools.getDist(), {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(results => {
                return results.json()
            }).then(data => {
                console.log(data)
                this.pagesCount = Math.ceil(data.length / this.pageSize);
                this.setState({ ListDistrubuteur: data })
            })
    }
    async blockDist(id, nom, prenom) {
        await fetch(tools.blockDistrubuteur(id), {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(results => {
                return results
            }).then(data => {
                alert("Le distrubuteur " + nom + " " + prenom + " a éte bloquer")
                this.setState({
                    ListDistrubuteur: this.state.ListDistrubuteur.filter(item => item.id !== id),
                });
            })
    }
    showRes(item) {
        if (item.blocked == 0) {
            return (
                <tr>
                    <td>{item.nom}</td>
                    <td>{item.pronom}</td>
                    <td>{item.nomCommercial}</td>
                    <td>{item.rc}</td>
                    <td>{item.matfiscal}</td>
                    <td>{item.telephone}</td>
                    <td>{item.ville}</td>
                    <td>{item.adresse}</td>
                    <td>{item.comm}</td>
                    <div style={{ paddingTop: 10 }}>
                        <Button style={{ height: 40, width: 40, alignItems: "center", justifyContent: "center" }} onClick={() => this.blockDist(item.id, item.nom, item.pronom)} className="btn-icon btn-2" color="danger" type="button">
                            <span className="btn-inner--icon">
                                <img style={{ marginLeft: -10 }} src={del} />
                            </span>
                        </Button>
                        <Button style={{ height: 40, width: 40, alignItems: "center", justifyContent: "center" }} onClick={() => this.props.history.replace(`/admin/Editdist`, { dist: item })} className="btn-icon btn-2" color="success" type="button">
                            <span className="btn-inner--icon">
                                <img style={{ marginLeft: -10 }} src={edit} />
                            </span>
                        </Button>
                    </div>
                </tr>
            )
        }
    }
    render() {
        const { currentPage } = this.state;
        return (
            <>
                <Header />
                {/* Page content */}
                <Container className="mt--7" fluid>
                    {/* Table */}
                    <Row>
                        <div className="col">
                            <Card className="shadow">
                                <CardHeader className="border-0">
                                    <h3 className="mb-0">Liste des distrubuteurs</h3>
                                </CardHeader>
                                <Table className="align-items-center table-flush" responsive>
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">Nom</th>
                                            <th scope="col">Prenom</th>
                                            <th scope="col">Nom Commercial</th>
                                            <th scope="col" >RC</th>
                                            <th scope="col">Matfiscal</th>
                                            <th scope="col">Téléphone</th>
                                            <th scope="col" >Ville</th>
                                            <th scope="col" >Adresse</th>
                                            <th scope="col" >Commission</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            this.state.ListDistrubuteur.slice(
                                                currentPage * this.pageSize,
                                                (currentPage + 1) * this.pageSize
                                            ).map((t) => this.showRes(t))
                                        }
                                    </tbody>
                                </Table>
                                <div className="pagination-wrapper">
                                    <Pagination style={{ padding: 10 }} className="pagination justify-content-end mb-0"
                                        listClassName="justify-content-end mb-0">
                                        <PaginationItem disabled={currentPage <= 0}>
                                            <PaginationLink
                                                onClick={e => this.handleClick(e, currentPage - 1)}
                                                previous
                                                href="#"
                                            />
                                        </PaginationItem>
                                        {[...Array(this.pagesCount)].map((page, i) =>
                                            <PaginationItem active={i === currentPage} key={i}>
                                                <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                                                    {i + 1}
                                                </PaginationLink>
                                            </PaginationItem>
                                        )}
                                        <PaginationItem disabled={currentPage >= this.pagesCount - 1}>
                                            <PaginationLink
                                                onClick={e => this.handleClick(e, currentPage + 1)}
                                                next
                                                href="#"
                                            />
                                        </PaginationItem>
                                    </Pagination>
                                </div>
                            </Card>
                        </div>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Listdist;
