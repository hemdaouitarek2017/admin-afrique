/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";
import Profile from "views/examples/Profile.js";
import Maps from "views/examples/Maps.js";
import Register from "views/examples/Register.js";
import Login from "views/examples/Login.js";
import Tables from "views/examples/Tables.js";
import Icons from "views/examples/Icons.js";
import Rentclient from "views/examples/Rentclient.js";
import EditRentClient from "views/examples/EditRentClient";
import Listdist from "views/examples/Listdist.js"
import Listrevendeur from "views/examples/Listrevendeur.js"
import Editdist from "views/examples/Editdist.js"
var routes = [
 
  {
    path: "/Editdist",
    name: "Modifier distrubuteur",
    icon: "ni ni-planet text-blue",
    component: Editdist,
    layout: "/admin"
  },
  {
    path: "/ModifierAppreil",
    name: "Modifier Appreil",
    icon: "ni ni-planet text-blue",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/Listdist",
    name: "Mise à jour Distrubuteur",
    icon: "ni ni-bullet-list-67 text-red",
    component: Listdist,
    layout: "/admin"
  },
  {
    path: "/Rentclient",
    name: "Liste des clients",
    icon: "ni ni-tv-2 text-primary",
    component: Rentclient,
    layout: "/admin"
  },
  {
    path: "/listegarantie",
    name: "Liste des contrats",
    icon: "ni ni-bullet-list-67 text-red",
    component: Maps,
    layout: "/admin"
  },

  {
    path: "/Listrevendeur",
    name: "Liste des Revendeurs",
    icon: "ni ni-bullet-list-67 text-red",
    component: Listrevendeur,
    layout: "/admin"
  },
  {
    path: "/tables",
    name: "Liste des appareils",
    icon: "ni ni-bullet-list-67 text-red",
    component: Tables,
    layout: "/admin"
  },
  {
    path: "/EditRentClient",
    name: "Edit",
    icon: "ni ni-bullet-list-67 text-red",
    component: EditRentClient,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth"
  },
  {
    path: "/register",
    name: "Register",
    icon: "ni ni-circle-08 text-pink",
    component: Register,
    layout: "/auth"
  }
];
export default routes;
